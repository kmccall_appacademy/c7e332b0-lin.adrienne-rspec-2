def measure(run_count = 1, &prc)
  start_time = Time.now
  run_count.times {prc.call}
  end_time = Time.now
  (Time.now - start_time) / run_count
end
