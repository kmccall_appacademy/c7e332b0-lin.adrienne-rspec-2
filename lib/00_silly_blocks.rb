def reverser(&prc)
  string = prc.call
  string.split.map(&:reverse).join(' ')
end

def adder(add_amount = 1, &prc)
  add_amount + prc.call
end

def repeater(repeat_count = 1, &prc)
  repeat_count.times(&prc)
end
